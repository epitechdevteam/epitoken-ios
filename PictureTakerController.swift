//
//  PictureTakerController.swift
//  epitoken
//
//  Created by Maxime on 11/05/15.
//  Copyright (c) 2015 Maxime de Dumast. All rights reserved.
//

import UIKit
import MobileCoreServices

class PictureTaker: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    var imagePicker: UIImagePickerController!
    var imageToSend: UIImage?
    var response: APIResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "postNoEventsFound:", name: "postNoEventsFoundError", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "progressReport:", name: "progressReporter", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "postSuccess:", name: "postSucessReporter", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "postNeedAction:", name: "postNeedActionReporter", object: nil)
        
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        dismissViewControllerAnimated(true, completion: nil)
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            ImageView.image = image
            imageToSend = image
            sendButton.enabled = true
        } else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            ImageView.image = image
            imageToSend = image
            sendButton.enabled = true
        }
    }
    
    func postSucess(notification: NSNotification) {
        let alertController = UIAlertController(title: "Success!", message: "You have been marked present for your activity", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    func progressReport(notification: NSNotification) {
        if let value = notification.userInfo!["value"] as? Float {
            dispatch_async(dispatch_get_main_queue()) {
                self.postProgressBar.progress = value
                if value == 1.0 {
                    self.postProgressBar.hidden = true
                }
            }
        }
    }
    
    @IBAction func unwindToSegue (segue : UIStoryboardSegue) {}
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "eventPickerSegue") {
            var vc = (segue.destinationViewController as! EventPicker)
            vc.apiResponse = self.response
        }
    }
    
    func postNeedAction(notification: NSNotification) {
        println("Hello hello")
        response = notification.userInfo!["response"] as? APIResponse
        dispatch_async(dispatch_get_main_queue()) {
            self.performSegueWithIdentifier("eventPickerSegue", sender: self)
        }
    }
    
    func postNoEventsFound(notification: NSNotification) {
        let status = notification.userInfo!["status"] as! String
        let token = notification.userInfo!["token"] as! String
        
        let alertController = UIAlertController(title: "Error", message: "You do not seem to have any events\nToken: \(token)", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
        
        dispatch_async(dispatch_get_main_queue()) {
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func selectPhotoButtonPressed() {
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        imagePicker.mediaTypes = [kUTTypeImage as NSString]
        imagePicker.allowsEditing = true
        
        dispatch_async(dispatch_get_main_queue()) {
            self.presentViewController(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func sendButtonPressed() {
        if let image = imageToSend {
            postProgressBar.hidden = false
            var api = EpitechApi()
            api.postImage(image)
        }
    }
    
    @IBAction func takePhotoButtonPressed() {
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
        imagePicker.mediaTypes = [kUTTypeImage as NSString]
        imagePicker.allowsEditing = true
        
        dispatch_async(dispatch_get_main_queue()) {
            self.presentViewController(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet weak var postProgressBar: UIProgressView!
}