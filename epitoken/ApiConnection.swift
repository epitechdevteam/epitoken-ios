//
//  ApiConnection.swift
//  epitoken
//
//  Created by Maxime on 01/05/15.
//  Copyright (c) 2015 Maxime de Dumast. All rights reserved.
//

import SwiftHTTP
import JSONJoy
import Foundation
import Locksmith
import UIKit

class EpitechApi {
    func tryLogin(login: NSString, pass: NSString) {
        var request = HTTPTask()
        request.responseSerializer = JSONResponseSerializer()
        request.GET("https://epitech-api.herokuapp.com/login", parameters: ["login" : login, "password": pass], completionHandler: {(response: HTTPResponse) in
            if let err = response.error {
                NSNotificationCenter.defaultCenter().postNotificationName("loginFailed", object: self)
                return
            }
            if let data = response.responseObject as? Dictionary<String,AnyObject> {
                if let token = data["token"] as? String {
                    let error = Locksmith.saveData(["token": token], forUserAccount: "authenticatedUser")
                    NSNotificationCenter.defaultCenter().postNotificationName("loginSender", object: self)
                    return
                }
            }
        })
    }
    
    func postTokenWithEvent(event: Event, tokenvalidationcode: String) {
        var request = HTTPTask()
        let (dictionary, error) = Locksmith.loadDataForUserAccount("authenticatedUser")
        
        if let dictionary = dictionary {
            if let token = dictionary["token"] as? String {
                request.responseSerializer = JSONResponseSerializer()
                request.GET("https://epitech-api.herokuapp.com/token", parameters: ["token": token, "scolaryear": event.scolaryear!, "codemodule": event.codemodule!, "codeinstance": event.codeinstance!, "codeacti": event.codeacti!, "codeevent": event.codeevent!, "tokenvalidationcode": tokenvalidationcode], completionHandler: { (response: HTTPResponse) in
                    if let err = response.error {
                        NSNotificationCenter.defaultCenter().postNotificationName("tokenFailureAction", object: nil)
                        return
                    }
                    if let obj: AnyObject = response.responseObject {
                        let resp = EpitechResponse(JSONDecoder(obj))
                        println("status: \(resp.error) error: \(resp.status)")
                        if let err = resp.error {
                            NSNotificationCenter.defaultCenter().postNotificationName("tokenFailureAction", object: nil)
                        } else if let status = resp.status {
                            if status == "ok" {
                                NSNotificationCenter.defaultCenter().postNotificationName("tokenSuccessAction", object: nil)
                            } else {
                                NSNotificationCenter.defaultCenter().postNotificationName("tokenFailureAction", object: nil)
                            }
                        }
                    }
                })
            }
        }
    }
    
    func postImage(image: UIImage) {
        
        var request = HTTPTask()
        let (dictionary, error) = Locksmith.loadDataForUserAccount("authenticatedUser")
        
        if let dictionary = dictionary {
            if let token = dictionary["token"] as? String {
                let params: Dictionary<String, AnyObject> = ["file": HTTPUpload(data: UIImagePNGRepresentation(image), fileName: "image.png", mimeType: "image/png")]
                
                request.responseSerializer = JSONResponseSerializer()
                
                request.upload("http://4242.co:5000/api/0.1/ocr_token/?login=\(token)", method: .POST, parameters: params, progress: {(value: Double) in
                    NSNotificationCenter.defaultCenter().postNotificationName("progressReporter", object: nil, userInfo: (["value": value]))},
                    
                    completionHandler: {(response: HTTPResponse) in
                        if let err = response.error {
                            println("Error occured")
                            return
                        }
                        if let obj: AnyObject = response.responseObject {
                            let resp = APIResponse(JSONDecoder(obj))
                            if resp.events == nil || resp.events?.count == 0 {
                                NSNotificationCenter.defaultCenter().postNotificationName("postNoEventsFoundError", object: nil, userInfo: (["status": "No events found", "token": resp.ocr_token!]))
                            } else if resp.events != nil && resp.events?.count > 0 {
                                NSNotificationCenter.defaultCenter().postNotificationName("postNeedActionReporter", object: nil, userInfo: (["response": resp]))
                            } else if let status = resp.status {
                                if status.rangeOfString("Token validated") != nil {
                                    NSNotificationCenter.defaultCenter().postNotificationName("postSucessReporter", object: nil, userInfo: nil)
                                }
                            }
                        }
                    }
                )
            }
        }
    }
}