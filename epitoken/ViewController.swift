//
//  ViewController.swift
//  epitoken
//
//  Created by Maxime on 01/05/15.
//  Copyright (c) 2015 Maxime de Dumast. All rights reserved.
//

import UIKit
import Locksmith

class ViewController: UIViewController {
    
    var loggedIn = false
    var api: EpitechApi = EpitechApi()
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        let (dictionary, error) = Locksmith.loadDataForUserAccount("authenticatedUser")
        if let dictionary = dictionary {
            if let token = dictionary["token"] as? String {
                println("User all ready authed: \(token)")
                let vc = self.storyboard?.instantiateViewControllerWithIdentifier("TabViewController") as! UIViewController
                self.presentViewController(vc, animated: true, completion: nil)
                
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loginReceiver", name: "loginSender", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loginFailedReceived", name: "loginFailed", object: nil)
    }
    
    func loginFailedReceived() {
        println("NO NO NO")
        loginInfoLabel.hidden = false
        loginInfoLabel.text = "Login failed"
    }
    
    func loginReceiver() {
        println("YippyKayYayMUTHAFUCKA")
        NSOperationQueue.mainQueue().addOperationWithBlock{
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("TabViewController") as! UIViewController
            self.presentViewController(vc, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginButton() {
        if loginTextField.text != "" && passTextField.text != "" {
            println("Querying API")
            api.tryLogin(loginTextField.text, pass: passTextField.text)
        }
    }
    
    @IBOutlet weak var loginInfoLabel: UILabel!
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passTextField: UITextField!
    
}

