//
//  EventPicker.swift
//  epitoken
//
//  Created by Maxime on 20/05/15.
//  Copyright (c) 2015 Maxime de Dumast. All rights reserved.
//

import Foundation
import UIKit

class EventPicker : UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    var apiResponse: APIResponse?
    var selectedEvent: Event?
    
    @IBOutlet weak var tokenTextField: UITextField!
    @IBOutlet weak var eventList: UIPickerView!
    @IBOutlet weak var sendButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tokenTextField.delegate = self
        tokenTextField.text = apiResponse?.ocr_token
        tokenTextField.clearsOnBeginEditing = false
        eventList.dataSource = self
        eventList.delegate = self
        self.addDoneButtonOnKeyboard()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "tokenFailure:", name: "tokenFailureAction", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "tokenSuccess:", name: "tokenSuccessAction", object: nil)
    }
    
    func tokenSucess(notification: NSNotification) {
        let alertController = UIAlertController(title: "Success!", message: "You have been marked present for your activity", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
        
        dispatch_async(dispatch_get_main_queue()){
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    func tokenFailure(notification: NSNotification) {
        let alertController = UIAlertController(title: "Error", message: "Something went wrong", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
        
        dispatch_async(dispatch_get_main_queue()){
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // Begin delegates for eventList
    // *****************************
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return self.apiResponse?.events?[row].acti_title
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedEvent = apiResponse?.events?[row]
    }
    // *****************************
    // End delegates for eventList
    
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if let tmp = apiResponse {
            if let events = tmp.events {
                return events.count
            }
        }
        return {0}()
    }

    func addDoneButtonOnKeyboard()
    {
        var doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, 320, 50))
        doneToolbar.barStyle = UIBarStyle.BlackTranslucent
        
        var flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        var done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: self, action: Selector("doneButtonAction"))
        
        var items = NSMutableArray()
        items.addObject(flexSpace)
        items.addObject(done)
        
        doneToolbar.items = items as [AnyObject]
        doneToolbar.sizeToFit()
        
        self.tokenTextField.inputAccessoryView = doneToolbar
        
    }
    
    @IBAction func tokenFieldValueChanged(sender: UITextField) {
        if count(tokenTextField.text) == 8 {
            sendButton.enabled = true
        } else {
            sendButton.enabled = false
        }
    }
    
    @IBAction func unwindToSegue (segue : UIStoryboardSegue) {}
    
    func doneButtonAction()
    {
        self.tokenTextField.resignFirstResponder()
        self.tokenTextField.resignFirstResponder()
    }
    
    @IBAction func sendButtonPressed() {
        var api = EpitechApi()
        
        if (count(tokenTextField.text) == 8) {
            println("Posting token")
            api.postTokenWithEvent(selectedEvent!, tokenvalidationcode: tokenTextField.text)
        } else {
            println("Token too short or long")
        }
    }
    

}
