//
//  JsonModels.swift
//  epitoken
//
//  Created by Maxime on 19/05/15.
//  Copyright (c) 2015 Maxime de Dumast. All rights reserved.
//

import JSONJoy

struct Event: JSONJoy {
    var scolaryear: String?
    var codemodule: String?
    var codeacti: String?
    var codeevent: String?
    var start: String?
    var end: String?
    var acti_title: String?
    var codeinstance: String?

    init(_ decoder: JSONDecoder) {
        scolaryear = decoder["scolaryear"].string
        codemodule = decoder["codemodule"].string
        codeacti = decoder["codeacti"].string
        codeinstance = decoder["codeinstance"].string
        codeevent = decoder["codeevent"].string
        start = decoder["start"].string
        end = decoder["end"].string
        acti_title = decoder["acti_title"].string
    }
}

class APIResponse : JSONJoy, AnyObject {
    var ocr_activity: String?
    var ocr_token: String?
    var status: String?
    var events: Array<Event>?

    init() {}
    required init(_ decoder: JSONDecoder) {
        if let ets = decoder["events"].array {
            events = Array<Event>()
            for eventDecoder in ets {
                events?.append(Event(eventDecoder))
            }
        }
        ocr_token = decoder["ocr_token"].string
        ocr_activity = decoder["ocr_activity"].string
        status = decoder["status"].string
    }
}

struct EpitechResponse : JSONJoy {
    var status: String?
    var error: String?
    
    init(_ decoder: JSONDecoder) {
        status = decoder["status"].string
        error = decoder["error"].string
    }
}